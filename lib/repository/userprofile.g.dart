// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'userprofile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    id: json['id'] as int,
    firstname: json['firstname'] as String,
    lastname: json['lastname'] as String,
    name: json['name'] as String,
    birthday: json['birthday'] as String,
    email: json['email'] as String,
    phoneNumber: json['phoneNumber'] as int,
    city: json['city'] as String,
    cardNumber: json['cardNumber'] as int,
    points: json['points'] as String,
    qrPath: json['qrPath'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'id': instance.id,
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'name': instance.name,
      'birthday': instance.birthday,
      'email': instance.email,
      'phoneNumber': instance.phoneNumber,
      'city': instance.city,
      'cardNumber': instance.cardNumber,
      'points': instance.points,
      'qrPath': instance.qrPath,
    };
