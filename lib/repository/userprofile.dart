import 'package:json_annotation/json_annotation.dart';

part 'userprofile.g.dart';

// User profile

@JsonSerializable()
class User {

  int id ;
  String firstname;
  String lastname;
  String name;
  String birthday;
  String email;
  int phoneNumber;
  String city;
  int cardNumber;
  String points;
  String qrPath;

  User({this.id, this.firstname, this.lastname, this.name, this.birthday, this.email, this.phoneNumber, 
  this.city, this.cardNumber, this.points, this.qrPath});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}