import "dart:async";

import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';
import 'package:vli_contact_tracing/ui/auth/model/usersignup.dart';


part 'networkcall.g.dart';

@RestApi(baseUrl: "https://myvictoryliner-api.2sds.com/api/vlp")
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @POST("/users")
  Future<UserSignUp> registerUser();
}
