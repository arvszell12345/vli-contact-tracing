// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'networkcall.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _ApiClient implements ApiClient {
  _ApiClient(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://myvictoryliner-api.2sds.com/api/vlp';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<UserSignUp> registerUser() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/users',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'POST',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = UserSignUp.fromJson(_result.data);
    return value;
  }
}