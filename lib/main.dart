import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/ui/dashboard/qrcode.dart';
import 'package:vli_contact_tracing/ui/dashboard/tab.dart';
import 'package:vli_contact_tracing/ui/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
    //   title: 'Flutter Demo',
    //   theme: ThemeData(
    //     primarySwatch: Colors.blue,
    //   ),
    //   //home: SplashScreen(),
    //   //   home: AdminSignInScreen(),
    //   home: ProvidedStylesExample(),
    // );
    return FutureBuilder(
        future: SharedPreferences.getInstance(),
        builder:
            (BuildContext context, AsyncSnapshot<SharedPreferences> prefs) {
          var x = prefs.data;
          if (prefs.hasData) {
            String tokenStr = (x.getString('token') ?? "");
            if (tokenStr.isEmpty) {
              return MaterialApp(home: SplashScreen());
            } else {
              return MaterialApp(home: ProvidedStylesExample());
            }
          }

          return MaterialApp(home: SplashScreen());
        });
  }
}
