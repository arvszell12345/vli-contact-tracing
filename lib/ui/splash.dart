import 'dart:developer';

import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/ui/dashboard/dashboard.dart';
import 'package:vli_contact_tracing/ui/auth/signup.dart';
import 'package:vli_contact_tracing/ui/profile/otp.dart';
import 'package:vli_contact_tracing/ui/widgets/textformfield.dart';
import 'package:vli_contact_tracing/ui/widgets/passformfield.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

import 'dashboard/tab.dart';
import 'dashboard/tabbar.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();

  ArsProgressDialog _progressDialog;

  Color _fc1;
  Color _fc2;

  @override
  Widget build(BuildContext context) {
    _progressDialog = ArsProgressDialog(context,
        blur: 2,
        backgroundColor: Color(0x33000000),
        animationDuration: Duration(milliseconds: 5000));

    Widget logoWidget() {
      return Column(
        children: <Widget>[
          Image.asset(
            'assets/ic_logo.png',
            width: 230.0,
            height: 110.0,
            fit: BoxFit.fill,
          ),
        ],
      );
    }

    Widget textWidget() {
      return Column(
        children: <Widget>[
          Text(
            "Sign in to your account",
            style: TextStyle(color: Colors.black, fontSize: 18),
          ),
        ],
      );
    }

    Widget emailTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc1 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.emailAddress,
          icon: Icons.person,
          textEditingController: email,
          hint: "Email",
          color: _fc1,
        ),
      );
    }

    Widget passwordTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc2 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: PassTextField(
          keyboardType: TextInputType.text,
          textEditingController: password,
          icon: Icons.lock,
          obscureText: false,
          hint: "Password",
          color: _fc2,
        ),
      );
    }

    Widget textWidgetForgotPassword() {
      return Column(
        children: <Widget>[
          TextButton(
            onPressed: () {
              log('data: clicked dito');
            },
            child: Align(
              alignment: Alignment.centerRight,
              child: Container(
                color: Colors.white,
                child: Text(
                  "Forgot Password?",
                  style: TextStyle(color: Colors.black),
                ),
              ),
            ),
          ),
        ],
      );
    }

    Widget loginbutton() {
      return RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: HexToColor('#EC202A'), width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(30.0),
        ),
        // onPressed: login,
        onPressed: () {
          //  singIn();
          signUpValidation(email.text, password.text);
        },
        padding: EdgeInsets.only(left: 45, right: 45, top: 16, bottom: 16),
        color: HexToColor('#EC202A'),
        child: Text(
          'LOGIN',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      );
    }

    Widget textWidgetDontHaveAccount() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Don't have an account?",
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              // Navigator.of(context).pushNamed(SIGN_UP);
              print("Routing to Sign up screen");
              signup();
            },
            child: Text(
              "Sign up",
              style: TextStyle(color: HexToColor('#EC202A'), fontSize: 18),
            ),
          )
        ],
      );
    }

    Widget textWidgetBottom() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "ⓒ Victory Liner Inc.",
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              // Navigator.of(context).pushNamed(SIGN_UP);
              print("Routing to Sign up screen");
            },
            child: Text(
              "Privacy policy.",
              style: TextStyle(color: HexToColor('#EC202A'), fontSize: 16),
            ),
          )
        ],
      );
    }

    return Material(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 70),
                  logoWidget(),
                  SizedBox(height: 10),
                  textWidget(),
                  SizedBox(height: 50),
                  emailTextFormField(),
                  SizedBox(height: 20),
                  passwordTextFormField(),
                  SizedBox(height: 10),
                  textWidgetForgotPassword(),
                  SizedBox(height: 15),
                  loginbutton(),
                  SizedBox(height: 15),
                  textWidgetDontHaveAccount(),
                  SizedBox(height: 15),
                  //   textWidgetAdminLoginTest(),
                  SizedBox(height: 100),
                  textWidgetBottom(),
                  SizedBox(height: 30),
                ],
              ),
            ),
          )),
    );
  }

  // FUNCTIONS AND METHODS HERE
  Future<void> signup() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SignUpScreen(),
        ));
  }

  // SIGN In function

  Future<void> singIn() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProvidedStylesExample(),
        ));
  }

  Future<void> signUpValidation(String emailStr, String passwordStr) async {
    print('email $emailStr');
    print('password $passwordStr');

    if (emailStr.isEmpty || passwordStr.isEmpty) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text("Please Enter Missing Fields."),
          );
        },
      );
    } else {
      _progressDialog.show();
      postRequest(emailStr, passwordStr);
    }
  }

  // POST METHOD

  Future<http.Response> postRequest(String emailStr, String passwordStr) async {
    var url = GeneralVariable.baseUrl + '/api/vlp/auth/users/signin/mobile';

    Map data = {
      'emailaddress': emailStr,
      'password': passwordStr,
    };
    //encode Map to JSON
    var body = json.encode(data);
    print('body  $body');
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    print('status${response.statusCode}');
    print('test${response.body}');

    if (response.statusCode == 200) {
      _progressDialog.dismiss();
      //    showOTP(response.body);
      Map data = jsonDecode(response.body);
      var token = data['Token'] ?? "";
      var forOTP = data['ForOTP'] ?? false;
      if (forOTP) {
        print("ALPHA  $token");
        showOTP(token, emailStr);
      } else {
        print("ALPHA  $token");

        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', token);
        // String tokenStr = (prefs.getString('token') ?? "");

        // print('BRAVO  $tokenStr');
        singIn();
      }
    } else {
      Map data = jsonDecode(response.body);
      var message = data['Message'];
      _progressDialog.dismiss();
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text(message),
          );
        },
      );
    }

    return response;
  }

  Future<void> showOTP(String token, String emailStr) async {
    //dio.options.headers["demo"] = "demo header";
    // final client = RestClient(dio);
    // client.registerUser(UserSignUp())
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OtpScreen(token, emailStr),
        ));
  }
}
