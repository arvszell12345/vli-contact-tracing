import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

import 'model/profileUserModel.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class QrCodeScreen extends StatefulWidget {
  @override
  _QrCodeScreenState createState() => _QrCodeScreenState();
}

class _QrCodeScreenState extends State<QrCodeScreen>
    with WidgetsBindingObserver {
  Future<ProfileUserModel> profileUserModel;
  bool valuefirst = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    profileUserModel = fetchProfile();
  }

  @override
  Widget build(BuildContext context) {
    Widget textWidget() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              "Good Morning!",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
        top: MediaQuery.of(context).size.height / 24,
        left: 35,
      );
    }

    Widget textWidget2() {
      return Positioned(
        child: Column(
          children: <Widget>[
            FutureBuilder<ProfileUserModel>(
                future: profileUserModel,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(snapshot.data.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold));
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                }),
          ],
        ),
        top: MediaQuery.of(context).size.height / 13,
        left: 35,
      );
    }

    Widget imageWidgets() {
      return Positioned(
        child: Column(
          children: <Widget>[
            FutureBuilder<ProfileUserModel>(
                future: profileUserModel,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    var hello = snapshot.data.qrPath;
                    print('BRAVO  $hello');
                    return Image.network(
                      "$hello",
                      width: MediaQuery.of(context).size.width / 1.5,
                      height: MediaQuery.of(context).size.height / 3,
                      fit: BoxFit.fill,
                    );
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                }),
          ],
        ),
        bottom: MediaQuery.of(context).size.height / 10,
        left: 0,
        right: 0,
      );
    }

    Widget createQrbutton() {
      return RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: HexToColor('#EC202A'), width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(30.0),
        ),
        // onPressed: login,
        onPressed: () {
          print("TAHAHA");
          //     DefaultTabController.of(context).animateTo(0);
        },
        padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        color: HexToColor('#EC202A'),
        child: Text(
          'GENERATE MY QR',
          style: TextStyle(color: Colors.white, fontSize: 13),
        ),
      );
    }

    Widget downloadQrbutton() {
      return RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: Colors.white, width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(30.0),
        ),
        // onPressed: login,
        onPressed: () {
          print("HAHA");
          //     DefaultTabController.of(context).animateTo(0);
        },
        padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        color: Colors.white,
        child: Text(
          'DOWNLOAD MY QR',
          style: TextStyle(color: HexToColor('#EC202A'), fontSize: 13),
        ),
      );
    }

    Widget topNavigation() {
      return Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height / 1.5,
            child: Container(
              // color: HexToColor('#EC202A'),
              child: Center(
                  //     child: Text("This is Sized Box"),
                  ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                color: HexToColor('#C51A27'),
                boxShadow: [
                  BoxShadow(color: HexToColor('#C51A27'), spreadRadius: 3),
                ],
              ),
              clipBehavior: Clip.antiAliasWithSaveLayer,
            ),
          ),
        ],
      );
    }

    Widget rowWidget() {
      return Positioned(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              createQrbutton(),
              SizedBox(width: 8),
              downloadQrbutton(),
            ],
          ),
        ),
        bottom: -30,
        right: 0,
        left: 0,
      );
    }

    Widget checkBoxWidget() {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Checkbox(
                checkColor: Colors.white,
                activeColor: Colors.red,
                value: this.valuefirst,
                onChanged: (bool value) {
                  setState(() {
                    this.valuefirst = value;
                  });
                },
              ),
              Text(
                "I agree to the Terms and Conditions of QR Code VLI",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ),
      );
    }

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: new Scaffold(
          backgroundColor: HexToColor('#FAFAFA'),
          body: Align(
            alignment: Alignment.topCenter,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Stack(
                      clipBehavior: Clip.none,
                      children: <Widget>[
                        topNavigation(),
                        textWidget(),
                        textWidget2(),
                        imageWidgets(),
                        rowWidget(),

                        // vliPointsNavigation(),
                        SizedBox(height: 50),
                        //  loginbutton(),
                      ],
                    ),
                    SizedBox(height: 30),
                    checkBoxWidget(),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Future<ProfileUserModel> fetchProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String tokenStr = (prefs.getString('token') ?? "");

    var url = GeneralVariable.baseUrl + '/api/vlp/users/profile';
    var response = await http.get(
      url,
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $tokenStr',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return ProfileUserModel.fromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      // throw Exception('Failed to load album');
    }
  }
}
