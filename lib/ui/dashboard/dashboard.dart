import 'package:flutter/material.dart';
import 'package:imagebutton/imagebutton.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/ui/dashboard/model/profileUserModel.dart';
import 'package:vli_contact_tracing/ui/dashboard/qrcode.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen>
    with WidgetsBindingObserver {
  Future<ProfileUserModel> profileUserModel;
  int _index = 1;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    profileUserModel = fetchProfile();
  }

  @override
  Widget build(BuildContext context) {
    Widget textWidget() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              "Good Morning!",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
        top: MediaQuery.of(context).size.height / 24,
        left: 35,
      );
    }

    Widget textWidget2() {
      return Positioned(
        child: Column(
          children: <Widget>[
            FutureBuilder<ProfileUserModel>(
                future: profileUserModel,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(snapshot.data.name,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold));
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                }),
          ],
        ),
        bottom: MediaQuery.of(context).size.height / 15,
        left: 35,
      );
    }

    Widget points() {
      return FutureBuilder<ProfileUserModel>(
              future: profileUserModel,
              builder: (context, snapshot) {
                if (snapshot.hasData)
                if (snapshot.data.points != null) {
                  return RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(text: snapshot.data.points.toString(),
                          style: TextStyle(color: HexToColor('#EC202A'), fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        WidgetSpan(
                          child: Transform.translate(
                            offset: const Offset(2, -13),
                            child: Text('PTS',
                              textScaleFactor: 0.9,
                              style: TextStyle(color: HexToColor('#EC202A')),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                } else if (snapshot.hasData)
                  if (snapshot.data.points == null) {
                  return RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(text: '0',
                          style: TextStyle(color: HexToColor('#EC202A'), fontSize: 25, fontWeight: FontWeight.bold),
                        ),
                        WidgetSpan(
                          child: Transform.translate(
                            offset: const Offset(2, -13),
                            child: Text('PTS',
                              textScaleFactor: 0.9,
                              style: TextStyle(color: HexToColor('#EC202A')),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }

                // By default, show a loading spinner.
                return CircularProgressIndicator();
              });
    }

    Widget loginbutton() {
      return RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: HexToColor('#EC202A'), width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(30.0),
        ),
        // onPressed: login,
        onPressed: () {
          DefaultTabController.of(context).animateTo(0);
        },
        padding: EdgeInsets.only(left: 45, right: 45, top: 16, bottom: 16),
        color: HexToColor('#EC202A'),
        child: Text(
          'LOGIN',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      );
    }

    Widget qrbutton() {
      return Positioned(
        child: ImageButton(
          children: <Widget>[],
          width: MediaQuery.of(context).size.height / 14,
          height: MediaQuery.of(context).size.width / 7,
          pressedImage: Image.asset(
            "assets/ic_scanner.png",
          ),
          unpressedImage: Image.asset("assets/ic_scanner.png"),
          onTap: () {
            print('test');
            // showQrCode();
            Navigator.of(context).push(_createRoute());
          },
        ),
        bottom: MediaQuery.of(context).size.height / 14,
        right: MediaQuery.of(context).size.height / 33,
      );
    }

    Widget vliNav() {
      return Positioned(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                  height: MediaQuery.of(context).size.height / 12,
                  width: MediaQuery.of(context).size.width / 1.2,
                  child: RaisedButton(
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                      side: BorderSide(
                          color: Colors.white,
                          width: 1,
                          style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                    // onPressed: login,
                    onPressed: () {
                      DefaultTabController.of(context).animateTo(0);
                    },
                    padding: EdgeInsets.only(
                        left: 45, right: 45, top: 16, bottom: 16),
                    color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('VLI Points',
                          style: TextStyle(color: HexToColor('#EC202A'), fontSize: 17),
                        ),
                        points(),
                      ],
                    ),
                  ),),
            ],
          ),
        ),
        bottom: -35,
        right: 0,
        left: 0,
      );
    }

    Widget vliTopUp() {
      return Positioned(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height / 30,
                width: MediaQuery.of(context).size.width / 4.4,
                child: RaisedButton(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                  // onPressed: login,
                  onPressed: () {
                    DefaultTabController.of(context).animateTo(0);
                  },
                  color: Colors.yellow,
                  child: Text('+ TOP UP', style: TextStyle(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.width / 30,),)
                ),),
            ],
          ),
        ),
        bottom: -50,
        right: 0,
        left: 200,
      );
    }

    Widget vliPointsNavigation() {
      return Positioned(
        child: Center(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height / 12,
                width: MediaQuery.of(context).size.width / 1.5,
                child: Container(
                  // padding: EdgeInsets.symmetric(horizontal: 0, vertical: -20),
                  // color: HexToColor('#EC202A'),
                  child: Center(
                      // child: Text("This is Sized Box"),
                      ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(color: Colors.white, spreadRadius: 1),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }

    Widget announcements() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 40),
            child: Text('Announcement',
              style: TextStyle(
                color: HexToColor('#EC202A'),
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 150,
            child: PageView.builder(
              itemCount: 4,
              controller: PageController(viewportFraction: 0.8),
              onPageChanged: (int index) => setState(() => _index = index),
              itemBuilder: (_, i) {
                if (i == 0) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 4,
                          child: Container(
                            child: Center(),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                topLeft: Radius.circular(10),
                              ),
                              color: HexToColor('#EC202A'),
                            ),
                          ),
                        ),
                        Expanded(
                          child:Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Now Available CUBAO-BAGUIO',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              Text('1'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }
                else if (i == 1) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('2'),
                    ),);
                }
                else if (i == 2) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('3'),
                    ),);
                }
                else if (i == 3) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('4'),
                    ),);
                }
                else return null;
              },
            ),
          ),
        ],
        );
    }

    Widget availableTrips() {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 40),
            child: Text('Available Trips',
              style: TextStyle(
                  color: HexToColor('#EC202A'),
                  fontSize: 17,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          SizedBox(height: 10),
          Container(
            height: 250,
            child: PageView.builder(
              itemCount: 4,
              controller: PageController(viewportFraction: 0.8),
              onPageChanged: (int index) => setState(() => _index = index),
              itemBuilder: (_, i) {
                if (i == 0) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 6.5,
                          child: Container(
                            child: Center(),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(10)),
                              color: HexToColor('#EC202A'),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Expanded(child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 5),
                                  child: Image.asset('assets/ic_logo_4.png',
                                    width: 25,
                                  ),
                                ),
                                SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    RichText(
                                      text: TextSpan(
                                        children: <TextSpan>[
                                          TextSpan(text:'Cubao',
                                            style: TextStyle(
                                              color: HexToColor('#EC202A'),
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          TextSpan(text: ' ∙∙35m∙∙',
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text('2'),
                                  ],
                                ),
                                SizedBox(width: 10),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Baguio',
                                      style: TextStyle(
                                        color: HexToColor('#EC202A'),
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    SizedBox(height: 10),
                                    Text('2'),
                                  ],
                                ),
                                Expanded(
                                  child: SizedBox(width: 0),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: RichText(
                                    text: TextSpan(
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.black,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(text: '24/',
                                          style: TextStyle(color: Colors.grey),
                                        ),
                                        TextSpan(text: '60'),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 20),
                                  child: Text('P1500.00',
                                    style: TextStyle(
                                      color: HexToColor('#EC202A'),
                                      fontSize: 20,
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20),
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50.0),
                                  ),
                                  onPressed: () {},
                                  padding: EdgeInsets.symmetric(horizontal: 30),
                                  color: HexToColor('#EC202A'),
                                  child: Text('BOOK NOW',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),),
                      ],
                    ),);
                }
                else if (i == 1) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('2'),
                    ),);
                }
                else if (i == 2) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('3'),
                    ),);
                }
                else if (i == 3) {
                  return Card(
                    elevation: 6,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text('4'),
                    ),);
                }
                else return null;
              },
            ),
          ),
        ],
      );
    }

    Widget promos() {
      return Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Promos',
                  style: TextStyle(
                    color: HexToColor('#EC202A'),
                    fontSize: 17,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextButton(
                  onPressed: () {},
                  child: Text('VIEW ALL PROMOS >',
                    style: TextStyle(
                      color: HexToColor('#EC202A'),
                      fontSize: 10,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Image.asset('assets/ic_logo_3.png',
            color: HexToColor('#EC202A'),
          ),
        ],
      );
    }

    Widget topNavigation() {
      return Stack(
        clipBehavior: Clip.none,
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height / 6,
            child: Container(
              // color: HexToColor('#EC202A'),
              child: Center(
                //     child: Text("This is Sized Box"),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                color: HexToColor('#EC202A'),
                boxShadow: [
                  BoxShadow(color: HexToColor('#EC202A'), spreadRadius: 3),
                ],
              ),
              // clipBehavior: Clip.antiAliasWithSaveLayer,
            ),
          ),
          qrbutton(),
          vliNav(),
          vliTopUp(),
          textWidget(),
          textWidget2(),
        ],
      );
    }

    // return Stack(
    //   clipBehavior: Clip.none,
    //   children: <Widget>[
    //     Align(
    //       alignment: Alignment.topCenter,
    //       child: Container(
    //         padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
    //         child: SingleChildScrollView(
    //           child: Column(
    //             children: <Widget>[
    //               topNav(),
    //               vliNav(),
    //               SizedBox(height: 50),
    //               loginbutton(),
    //             ],
    //           ),
    //         ),
    //       ),
    //     ),
    //   ],
    // );




    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  topNavigation(),
                  SizedBox(height: MediaQuery.of(context).size.height / 12),
                  announcements(),
                  SizedBox(height: 10),
                  availableTrips(),
                  promos(),
                  // vliPointsNavigation(),
                  //  loginbutton(),
                ],
              ),
            ],
          ),
        ),
      ),
    );

    // return Center(
    //   child: Stack(
    //     clipBehavior: Clip.none,
    //     children: <Widget>[
    //       Container(
    //         color: Colors.amber,
    //         height: 150,
    //         width: 150,
    //       ),
    //       Positioned(
    //         child: FloatingActionButton(
    //           child: Icon(Icons.add_a_photo),
    //           onPressed: () {
    //             print('FAB tapped!');
    //           },
    //           backgroundColor: Colors.blueGrey,
    //         ),
    //         right: 0,
    //         left: 0,
    //         bottom: -26,
    //       ),
    //     ],
    //   ),
    // );
  }

  // FUNCTIONS AND METHODS
  //
  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) => QrCodeScreen(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;

        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  Future<void> showQrCode() async {
    //dio.options.headers["demo"] = "demo header";
    // final client = RestClient(dio);
    // client.registerUser(UserSignUp())
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => QrCodeScreen(),
        ));
  }

  Future<ProfileUserModel> fetchProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String tokenStr = (prefs.getString('token') ?? "");

    var url = GeneralVariable.baseUrl + '/api/vlp/users/profile';
    var response = await http.get(
      url,
      headers: {
        "Content-Type": "application/json",
        'Authorization': 'Bearer $tokenStr',
      },
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return ProfileUserModel.fromJson(jsonDecode(response.body));
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      // throw Exception('Failed to load album');
      return ProfileUserModel.fromJson(jsonDecode(response.body));
    }
  }
}