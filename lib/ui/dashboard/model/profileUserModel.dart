import 'package:flutter/material.dart';

class ProfileUserModel {
  final int id;
  final String firstName;
  final String lastName;
  final String name;
  final String birthday;
  final String email;
  final String phoneNumber;
  final String city;
  final int cardNumber;
  final int points;
  final String qrPath;

  ProfileUserModel({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.name,
    @required this.birthday,
    @required this.email,
    @required this.phoneNumber,
    @required this.city,
    @required this.cardNumber,
    @required this.points,
    @required this.qrPath,
  });

  factory ProfileUserModel.fromJson(Map<String, dynamic> json) {
    return ProfileUserModel(
      id: json['Id'],
      firstName: json['Firstname'],
      lastName: json['Lastname'],
      name: json['Name'],
      birthday: json['Birthday'],
      email: json['Email'],
      phoneNumber: json['PhoneNumber'],
      city: json['City'],
      cardNumber: json['CardNumber'],
      points: json['Points'],
      qrPath: json['QRPath'],
    );
  }
}
