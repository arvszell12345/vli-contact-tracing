import 'package:flutter/material.dart';
import 'package:vli_contact_tracing/ui/dashboard/dashboard.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

class TabBarScreen extends StatefulWidget {
  @override
  _TabBarScreenState createState() => _TabBarScreenState();
}

class _TabBarScreenState extends State<TabBarScreen> {
  @override
  Widget build(BuildContext context) {
    Widget menu() {
      return Container(
        color: Colors.white,
        child: TabBar(
          labelColor: HexToColor('#EC202A'),
          unselectedLabelColor: HexToColor('#EC202A'),
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorPadding: EdgeInsets.all(5.0),
          indicatorColor: HexToColor('#EC202A'),
          tabs: [
            Tab(
              text: "Home",
              icon: Image.asset(
                'assets/ic_home.png',
                width: 25.0,
                height: 25.0,
                fit: BoxFit.fill,
              ),
            ),
            Tab(
              text: "Profile",
              icon: Image.asset(
                'assets/ic_profile.png',
                width: 25.0,
                height: 25.0,
                fit: BoxFit.fill,
              ),
            ),
            Tab(
              text: "Rewards",
              icon: Image.asset(
                'assets/ic_rewards.png',
                width: 25.0,
                height: 25.0,
                fit: BoxFit.fill,
              ),
            ),
            Tab(
              text: "Rewards",
              icon: Image.asset(
                'assets/ic_settings.png',
                width: 25.0,
                height: 25.0,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ),
      );
    }

    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.white,
              title: Center(
                child: Image.asset('assets/ic_logo_2.png',
                    width: 230.0,
                    height: 50.0,
                    fit: BoxFit.fill,
                    alignment: Alignment.center),
              )),
          bottomNavigationBar: menu(),
          body: TabBarView(
            children: [
              DashboardScreen(),
              Container(child: Icon(Icons.directions_transit)),
              Container(child: Icon(Icons.directions_bike)),
              Container(child: Icon(Icons.directions_bike)),
            ],
          ),
        ),
      ),
    );
  }
}
