import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/ui/dashboard/dashboard.dart';
import 'package:vli_contact_tracing/ui/dashboard/profile.dart';
import 'package:vli_contact_tracing/ui/dashboard/rewards.dart';
import 'package:vli_contact_tracing/ui/splash.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';
import 'package:vli_contact_tracing/ui/dashboard/tab.dart';
import 'package:vli_contact_tracing/ui/dashboard/tabRewards.dart';

class ProvidedStylesProfile extends StatefulWidget {
  @override
  _ProvidedStylesProfileState createState() => _ProvidedStylesProfileState();
}

class _ProvidedStylesProfileState extends State<ProvidedStylesProfile> {
  PersistentTabController _controller;
  bool _hideNavBar;

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 1);
    _hideNavBar = false;
  }

  List<Widget> _buildScreens() {
    return [
      DashboardScreen(),
      ProfileScreen(),
      RewardsScreen(),
      //  Container(child: Icon(Icons.directions_bike)),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home),
        title: "Home",
        activeColorPrimary: HexToColor('#EC202A'),
        inactiveColorPrimary: HexToColor('#EC202A'),
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.person),
        title: ("Profile"),
        activeColorPrimary: HexToColor('#EC202A'),
        inactiveColorPrimary: HexToColor('#EC202A'),
        routeAndNavigatorSettings: RouteAndNavigatorSettings(
          initialRoute: '/',
          routes: {
            // '/first': (context) => MainScreen2(),
            // '/second': (context) => MainScreen3(),
          },
        ),
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.rosette),
        title: ("Rewards"),
        activeColorPrimary: HexToColor('#EC202A'),
        inactiveColorPrimary: HexToColor('#EC202A'),
        routeAndNavigatorSettings: RouteAndNavigatorSettings(
          initialRoute: '/',
          routes: {
            // '/first': (context) => MainScreen2(),
            // '/second': (context) => MainScreen3(),
          },
        ),
      ),
      // PersistentBottomNavBarItem(
      //   icon: Icon(CupertinoIcons.settings),
      //   title: ("Settings"),
      //   activeColorPrimary: HexToColor('#EC202A'),
      //   inactiveColorPrimary: HexToColor('#EC202A'),
      //   routeAndNavigatorSettings: RouteAndNavigatorSettings(
      //     initialRoute: '/',
      //     routes: {
      //       // '/first': (context) => MainScreen2(),
      //       // '/second': (context) => MainScreen3(),
      //     },
      //   ),
      // ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            backgroundColor: Colors.white,
            iconTheme: IconThemeData(color: HexToColor('#EC202A'),
            ),
            title: ConstrainedBox(
              constraints: BoxConstraints(maxHeight: 70, maxWidth: 230),
              child: Image.asset(
                'assets/ic_logo_2.png',
                width: 230.0,
                height: 70.0,
                // fit: BoxFit.fill,
              ),
            ),
          ),
          drawer: Drawer(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 0, vertical: 50),
              child: Column(
                children: <Widget>[
                  Image.asset(
                    'assets/ic_logo_3.png',
                    color: HexToColor('#EC202A'),
                    width: 230.0,
                    height: 70.0,
                    // fit: BoxFit.fill,
                  ),
                  SizedBox(height: 50),
                  ListTile(
                    leading: Icon(
                      CupertinoIcons.home,
                      color: HexToColor('#EC202A'),
                      size: 30,
                    ),
                    title: Text(
                      'Home',
                      style: TextStyle(
                        color: HexToColor('#EC202A'),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProvidedStylesExample()),
                      );
                      //   DashboardScreen();
                      // Navigator.pop(context);
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      CupertinoIcons.person,
                      color: HexToColor('#EC202A'),
                      size: 30,
                    ),
                    title: Text(
                      'Profile',
                      style: TextStyle(
                        color: HexToColor('#EC202A'),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProvidedStylesProfile()),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      CupertinoIcons.rosette,
                      color: HexToColor('#EC202A'),
                      size: 30,
                    ),
                    title: Text(
                      'Rewards',
                      style: TextStyle(
                        color: HexToColor('#EC202A'),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProvidedStylesRewards()),
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.exit_to_app,
                      color: HexToColor('#EC202A'),
                      size: 30,
                    ),
                    title: Text(
                      'Logout',
                      style: TextStyle(
                        color: HexToColor('#EC202A'),
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => SplashScreen(),
                        ),
                            (Route route) => false,
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          body: PersistentTabView(
            context,
            controller: _controller,
            screens: _buildScreens(),
            items: _navBarsItems(),
            confineInSafeArea: true,
            backgroundColor: Colors.white,
            handleAndroidBackButtonPress: true,
            resizeToAvoidBottomInset: true,
            stateManagement: true,
            navBarHeight: MediaQuery.of(context).viewInsets.bottom > 0
                ? 0.0
                : kBottomNavigationBarHeight,
            hideNavigationBarWhenKeyboardShows: true,
            margin: EdgeInsets.all(0.0),
            popActionScreens: PopActionScreensType.all,
            bottomScreenMargin: 0.0,
            onWillPop: (context) async {
              await showDialog(
                context: context,
                useSafeArea: true,
                builder: (context) => Container(
                  height: 50.0,
                  width: 50.0,
                  color: Colors.white,
                  child: ElevatedButton(
                    child: Text("Close"),
                    onPressed: () {
                      Navigator.pop(context);
                      },
                  ),
                ),
              );
              return false;
              },
        // selectedTabScreenContext: (context) {
        //   testContext = context;
        // },
            hideNavigationBar: _hideNavBar,
            decoration: NavBarDecoration(
              colorBehindNavBar: Colors.indigo,
              borderRadius: BorderRadius.circular(20.0),
            ),
            popAllScreensOnTapOfSelectedTab: true,
            itemAnimationProperties: ItemAnimationProperties(
              duration: Duration(milliseconds: 400),
              curve: Curves.ease,
            ),
            screenTransitionAnimation: ScreenTransitionAnimation(
              animateTabTransition: true,
              curve: Curves.ease,
              duration: Duration(milliseconds: 200),
            ),
            navBarStyle: NavBarStyle.style6, // Choose the nav bar style with this property
          ),
        ),
    );
  }

  Future<void> showLoginUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');

    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SplashScreen(),
        ));
  }
}

class MainScreen {}
