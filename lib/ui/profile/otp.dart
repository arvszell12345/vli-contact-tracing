import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vli_contact_tracing/ui/dashboard/dashboard.dart';
import 'package:vli_contact_tracing/ui/dashboard/tab.dart';
import 'package:vli_contact_tracing/ui/widgets/textformfield2.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class OtpScreen extends StatefulWidget {
  final String token;
  final String email;

  OtpScreen(this.token, this.email, {Key key}) : super(key: key);

  @override
  _OtpScreenState createState() => _OtpScreenState(token, email);
}

class _OtpScreenState extends State<OtpScreen> with WidgetsBindingObserver {
  final String token;
  final String emai;

  final otp = TextEditingController();
  ArsProgressDialog _progressDialog;

  _OtpScreenState(this.token, this.emai);
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    print('token  $token');
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    otp.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _progressDialog = ArsProgressDialog(context,
        blur: 2,
        backgroundColor: Color(0x33000000),
        animationDuration: Duration(milliseconds: 5000));

    Widget topNavigation() {
      return Column(
        children: <Widget>[
          SizedBox(
            height: MediaQuery.of(context).size.height / 2.3,
            child: Container(
              // color: HexToColor('#EC202A'),
              child: Center(
                  // child: Text(token),
                  ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(30),
                  bottomRight: Radius.circular(30),
                ),
                color: HexToColor('#EC202A'),
                boxShadow: [
                  BoxShadow(color: HexToColor('#EC202A'), spreadRadius: 3),
                ],
              ),
              clipBehavior: Clip.antiAliasWithSaveLayer,
            ),
          ),
        ],
      );
    }

    Widget textWidget() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              "Enter verification code sent to your registered email address.",
              maxLines: 2,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            ),
          ],
        ),
        bottom: MediaQuery.of(context).size.height / 10,
        left: 60,
        right: 60,
      );
    }

    Widget textWidget2() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              emai,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        bottom: MediaQuery.of(context).size.height / 20,
        left: 0,
        right: 0,
      );
    }

    Widget textWidget3() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              "Good Morning!",
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.normal,
              ),
            ),
          ],
        ),
        top: MediaQuery.of(context).size.height / 20,
        left: 30,
        //  right: 60,
      );
    }

    Widget textWidget4() {
      return Positioned(
        child: Column(
          children: <Widget>[
            Text(
              "Arvin Lemuel M Cabunoc",
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        top: MediaQuery.of(context).size.height / 13,
        left: 30,
        //  right: 60,
      );
    }

    Widget otpTextFormField() {
      return Positioned(
        child: Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width / 1.5,
              child: CustomTextField2(
                keyboardType: TextInputType.text,
                textEditingController: otp,
                //icon: Icons.person_outline,
                hint: "ONE TIME PIN",
              ),
            ),
          ],
        ),
        bottom: -35,
        left: 0,
        right: 0,
      );
    }

    Widget loginbutton() {
      return Container(
        //  padding: EdgeInsets.symmetric(horizontal: 0, vertical: 230),
        child: Center(
          child: RaisedButton(
            shape: RoundedRectangleBorder(
              side: BorderSide(
                  color: HexToColor('#EC202A'),
                  width: 1,
                  style: BorderStyle.solid),
              borderRadius: BorderRadius.circular(30.0),
            ),
            // onPressed: login,
            onPressed: () {
              print('hello');
              otpValidation(otp.text, token);
            },
            padding: EdgeInsets.only(left: 45, right: 45, top: 16, bottom: 16),
            color: HexToColor('#EC202A'),
            child: Text(
              'SUBMIT',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
        ),

        // bottom: -105,
        // right: 0,
        // left: 0,
      );
    }

    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          title: Center(
            child: Image.asset(
              'assets/ic_logo_2.png',
              width: 230.0,
              height: 50.0,
              // fit: BoxFit.fill,
            ),
          )),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 0.0, vertical: 0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  topNavigation(),
                  otpTextFormField(),
                  textWidget(),
                  textWidget2(),
                  textWidget3(),
                  textWidget4()
                ],
              ),
              SizedBox(height: 50),
              loginbutton(),
            ],
          ),
        ),
      ),
    ));
  }

  Future<void> otpValidation(String otpStr, String tokenStr) async {
    print('otp  $otpStr');
    if (otpStr.isEmpty) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text("Please Enter Missing Fields."),
          );
        },
      );
    } else {
      _progressDialog.show();
      postRequest(tokenStr, otpStr);
    }
    return true;
  }

  Future<http.Response> postRequest(String token, String otpStr) async {
    var url = GeneralVariable.baseUrl + '/api/vlp/users/otp';

    Map data = {
      'OTP': otpStr,
    };
    //encode Map to JSON
    var body = json.encode(data);
    final tokenStr = token.replaceAll(RegExp('\\"'), '');
    print('token  $tokenStr');
    print('body  $body');
    var response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'Bearer $tokenStr',
        },
        body: body);
    print('status${response.statusCode}');
    print('test${response.body}');
    if (response.statusCode == 200) {
      _progressDialog.dismiss();
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('token', token);
      dashBoard();
    } else {
      _progressDialog.dismiss();
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text(response.body),
          );
        },
      );
    }
    return response;
  }

  Future<void> dashBoard() async {
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProvidedStylesExample(),
        ));
  }
}
