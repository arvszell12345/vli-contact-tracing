// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'usersignup.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSignUp _$UserSignUpFromJson(Map<String, dynamic> json) {
  return UserSignUp(
    firstname: json['firstname'] as String,
    lastname: json['lastname'] as String,
    birthday: json['birthday'] as String,
    emailadd: json['emailadd'] as String,
    password: json['password'] as String,
    phoneno: json['phoneno'] as int,
    city: json['city'] as String,
    cardno: json['cardno'] as int,
    terms: json['terms'] as bool,
  );
}

Map<String, dynamic> _$UserSignUpToJson(UserSignUp instance) =>
    <String, dynamic>{
      'firstname': instance.firstname,
      'lastname': instance.lastname,
      'birthday': instance.birthday,
      'emailadd': instance.emailadd,
      'password': instance.password,
      'phoneno': instance.phoneno,
      'city': instance.city,
      'cardno': instance.cardno,
      'terms': instance.terms,
    };

ResponseData _$ResponseDataFromJson(Map<String, dynamic> json) {
  return ResponseData(
    code: json['code'] as int,
    meta: json['meta'],
    data: json['data'] as List,
  );
}

Map<String, dynamic> _$ResponseDataToJson(ResponseData instance) =>
    <String, dynamic>{
      'code': instance.code,
      'meta': instance.meta,
      'data': instance.data,
    };
