import 'package:json_annotation/json_annotation.dart';

part 'usersignup.g.dart';

@JsonSerializable()
class UserSignUp {
  String firstname;
  String lastname;
  String birthday;
  String emailadd;
  String password;
  int phoneno;
  String city;
  int cardno;
  bool terms;

  UserSignUp(
      {this.firstname,
      this.lastname,
      this.birthday,
      this.emailadd,
      this.password,
      this.phoneno,
      this.city,
      this.cardno,
      this.terms});

  factory UserSignUp.fromJson(Map<String, dynamic> json) =>
      _$UserSignUpFromJson(json);
  Map<String, dynamic> toJson() => _$UserSignUpToJson(this);
}

@JsonSerializable()
class ResponseData{
  int code;
  dynamic meta;
  List<dynamic>data;
  ResponseData({this.code, this.meta, this.data});
  factory ResponseData.fromJson(Map<String, dynamic> json) => _$ResponseDataFromJson(json);
  Map<String, dynamic> toJson() => _$ResponseDataToJson(this);

}