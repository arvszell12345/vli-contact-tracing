import 'package:ars_progress_dialog/ars_progress_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vli_contact_tracing/retrofit/networkcall.dart';
import 'package:vli_contact_tracing/ui/profile/otp.dart';

import 'package:vli_contact_tracing/ui/splash.dart';
import 'package:vli_contact_tracing/ui/widgets/numbertextformfield.dart';
import 'package:vli_contact_tracing/ui/widgets/textformfield.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';
import 'package:vli_contact_tracing/ui/widgets/passformfield.dart';

import 'model/usersignup.dart';
import 'dart:developer' as developer;
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter_recaptcha_v2/flutter_recaptcha_v2.dart';

class SignUpScreen extends StatefulWidget {

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String verifyResult = "Click here to verify";
  RecaptchaV2Controller recaptchaV2Controller = RecaptchaV2Controller();

  bool valuefirst = false;
  bool userEmailValidate = false;

  final email = TextEditingController();
  final firstName = TextEditingController();
  final lastName = TextEditingController();
  final mobile = TextEditingController();
  final vlpNo = TextEditingController();
  final password = TextEditingController();
  final confirmPassword = TextEditingController();
  final address = TextEditingController();
  final birthday = TextEditingController();
  var recaptcha;

  ArsProgressDialog _progressDialog;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    email.dispose();
    firstName.dispose();
    lastName.dispose();
    mobile.dispose();
    vlpNo.dispose();
    password.dispose();
    confirmPassword.dispose();
    address.dispose();
    birthday.dispose();
    recaptcha.dispose();
    super.dispose();
  }

  Color _fc1;
  Color _fc2;
  Color _fc3;
  Color _fc4;
  Color _fc5;
  Color _fc6;
  Color _fc7;
  Color _fc8;
  Color _fc9;

  @override
  Widget build(BuildContext context) {
    _progressDialog = ArsProgressDialog(context,
        blur: 2,
        backgroundColor: Color(0x33000000),
        animationDuration: Duration(milliseconds: 5000));

    Widget textWidget() {
      return Column(
        children: <Widget>[
          Text(
            "Let's Get Started!",
            style: TextStyle(
                color: HexToColor('#EC202A'),
                fontSize: 28,
                fontWeight: FontWeight.bold),
          ),
        ],
      );
    }

    Widget textWidget2() {
      return Center(
          child: Padding(
        padding: EdgeInsets.only(left: 34, right: 34),
        child: Column(
          children: <Widget>[
            Text(
              "Create an account to Victory Linear Inc. to get all features",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ));
    }

    Widget emailTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc1 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.text,
          textEditingController: email,
          icon: Icons.email_outlined,
          hint: "Email",
          color: _fc1,
        ),
      );
    }

    Widget firstNameTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc2 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.text,
          textEditingController: firstName,
          icon: Icons.person_outline,
          hint: "First Name",
          color: _fc2,
        ),
      );
    }

    Widget lastNameTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc3 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.text,
          textEditingController: lastName,
          icon: Icons.person_outline,
          hint: "Last Name",
          color: _fc3,
        ),
      );
    }

    Widget contactNumberTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc4 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: NumberCustomTextField(
          maxNumber: 11,
          keyboardType: TextInputType.number,
          digitsOnly: FilteringTextInputFormatter.digitsOnly,
          textEditingController: mobile,
          icon: Icons.phone_outlined,
          hint: "Contact Number",
          color: _fc4,
        ),
      );
    }

    Widget cardNumberTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc5 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: NumberCustomTextField(
          maxNumber: 16,
          keyboardType: TextInputType.number,
          digitsOnly: FilteringTextInputFormatter.digitsOnly,
          textEditingController: vlpNo,
          icon: Icons.insert_invitation,
          hint: "VLP Card Number",
          color: _fc5,
        ),
      );
    }

    Widget passwordTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc6 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: PassTextField(
          keyboardType: TextInputType.text,
          textEditingController: password,
          icon: Icons.lock_outlined,
          hint: "Password",
          color: _fc6,
        ),
      );
    }

    Widget confirmPasswordTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc7 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: PassTextField(
            keyboardType: TextInputType.text,
            textEditingController: confirmPassword,
            icon: Icons.lock_outlined,
            hint: "Confirm Password",
            color: _fc7),
      );
    }

    Widget addressTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc8 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.text,
          textEditingController: address,
          icon: Icons.location_pin,
          hint: "Address",
          color: _fc8,
        ),
      );
    }

    Widget birthdayTextFormField() {
      return Focus(
        onFocusChange: (hasFocus) {
          setState(() => _fc9 = hasFocus ? HexToColor('#EC202A') : Colors.grey);
        },
        child: CustomTextField(
          keyboardType: TextInputType.text,
          textEditingController: birthday,
          icon: Icons.calendar_today_outlined,
          hint: "Birthday ",
          color: _fc9,
        ),
      );
    }

    Widget captchaVerify() {
      return ConstrainedBox(
        constraints: BoxConstraints.tightFor(width: MediaQuery.of(context).size.width, height: 50),
        child: ElevatedButton(
            child: Text(verifyResult,
              style: TextStyle(color: Colors.black),
            ),
            style: ElevatedButton.styleFrom(
              primary: Colors.white70,
              side: BorderSide(color: Colors.grey, width: 2),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
            ),
            onPressed: () {
              recaptchaV2Controller.show();
            },
      ),);
    }

    Widget captcha() {
      return RecaptchaV2(
        apiKey: "6LeCwZYUAAAAAJo8IVvGX9dH65Rw89vxaxErCeou",
        apiSecret: "6LeCwZYUAAAAAKGahIjwfOARevvRETgvwhPMKCs_",
        controller: recaptchaV2Controller,
        onVerifiedError: (err){
          print(err);
          },
        onVerifiedSuccessfully: (success) {
          setState(() {
            if (success) {
              verifyResult = "You've been verified successfully.";
            } else {
              verifyResult = "Failed to verify.";
            }
          });
          },
      );
    }

    Widget checkBoxWidget() {
      return Row(
        children: <Widget>[
          Checkbox(
            checkColor: Colors.white,
            activeColor: Colors.red,
            value: this.valuefirst,
            onChanged: (bool value) {
              setState(() {
                this.valuefirst = value;
              });
            },
          ),
          Text(
            "I have read PRIVACY POLICY and \n ACCEPT TERMS & CONDITIONS",
            style: TextStyle(
              color: Colors.black,
              fontSize: 14,
            ),
          ),
        ],
      );
    }

    Widget createbutton() {
      return RaisedButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
              color: HexToColor('#EC202A'), width: 1, style: BorderStyle.solid),
          borderRadius: BorderRadius.circular(30.0),
        ),
        // onPressed: login,
        onPressed: () {
          signUpValidation(
              email.text,
              firstName.text,
              lastName.text,
              mobile.text,
              vlpNo.text,
              password.text,
              confirmPassword.text,
              address.text,
              birthday.text,
              recaptcha);
        },
        padding: EdgeInsets.only(left: 45, right: 45, top: 16, bottom: 16),
        color: HexToColor('#EC202A'),
        child: Text(
          'CREATE',
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      );
    }

    Widget textWidgetDontHaveAccount() {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "Already have an account?",
            style: TextStyle(color: Colors.black, fontSize: 16),
          ),
          SizedBox(
            width: 5,
          ),
          GestureDetector(
            onTap: () {
              signIn();
              print("Routing to Sign up screen");
            },
            child: Text(
              "Sign In",
              style: TextStyle(color: HexToColor('#EC202A'), fontSize: 18),
            ),
          )
        ],
      );
    }

    return Material(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 0),
        child: Stack(
          children: [
            Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 100),
                    textWidget(),
                    SizedBox(height: 15),
                    textWidget2(),
                    SizedBox(height: 30),
                    emailTextFormField(),
                    SizedBox(height: 10),
                    firstNameTextFormField(),
                    SizedBox(height: 10),
                    lastNameTextFormField(),
                    SizedBox(height: 10),
                    contactNumberTextFormField(),
                    SizedBox(height: 10),
                    cardNumberTextFormField(),
                    SizedBox(height: 10),
                    passwordTextFormField(),
                    SizedBox(height: 10),
                    confirmPasswordTextFormField(),
                    SizedBox(height: 10),
                    addressTextFormField(),
                    SizedBox(height: 10),
                    birthdayTextFormField(),
                    SizedBox(height: 10),
                    captchaVerify(),
                    SizedBox(height: 10),
                    checkBoxWidget(),
                    SizedBox(height: 10),
                    createbutton(),
                    SizedBox(height: 15),
                    textWidgetDontHaveAccount(),
                    SizedBox(height: 50),
                  ],
                ),
              ),
            ),
            captcha(),
          ],
        ),
      ),
    );
  }

  // FUNCTIONS AND METHODS HERE
  Future<void> signIn() async {
    //dio.options.headers["demo"] = "demo header";
    // final client = RestClient(dio);
    // client.registerUser(UserSignUp())
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => SplashScreen(),
        ));
  }

  Future<void> showOTP(String tokenStr, String emailStr) async {
    //dio.options.headers["demo"] = "demo header";
    // final client = RestClient(dio);
    // client.registerUser(UserSignUp())
    await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => OtpScreen(tokenStr, emailStr),
        ));
  }

  // FutureBuilder<UserSignUp> _buildBody(BuildContext context) {
  //   final client = ApiClient(Dio(BaseOptions(contentType: "application/json")));
  //   return FutureBuilder<UserSignUp>(
  //     future: client.registerUser(),
  //     builder: (context, snapshot) {
  //       if (snapshot.connectionState == ConnectionState.done) {
  //         final UserSignUp posts = snapshot.data;
  //         return _buildPosts(context, posts);
  //       } else {
  //         return Center(
  //           child: CircularProgressIndicator(),
  //         );
  //       }
  //     },
  //   );
  // }

  Future<void> signUpValidation(
      String emailStr,
      String nameStr,
      String lastnameStr,
      String contactNumberStr,
      String vlpCardNumberStr,
      String passwordStr,
      String confirmPasswordStr,
      String addressStr,
      String birthdayStr,
      String recaptchaStr) async {
    print('email $emailStr');
    print('name $nameStr');
    print('lastname $lastnameStr');
    print('contact  $contactNumberStr');
    print('vlp  $vlpCardNumberStr');
    print('password $passwordStr');
    print('confirm pass $confirmPasswordStr');
    print('address $addressStr');
    print('birthday $birthdayStr');
    print('tokencaptcha $recaptchaStr');

    if (emailStr.isEmpty ||
        nameStr.isEmpty ||
        lastnameStr.isEmpty ||
        contactNumberStr.isEmpty ||
        passwordStr.isEmpty ||
        confirmPasswordStr.isEmpty ||
        addressStr.isEmpty ||
        birthdayStr.isEmpty ||
        recaptchaStr.isEmpty) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text("Please Enter Missing Fields."),
          );
        },
      );
    } else if (passwordStr != confirmPasswordStr) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text("Your password is mismatch"),
          );
        },
      );
    } else {
      _progressDialog.show();
      postRequest(emailStr, nameStr, lastnameStr, contactNumberStr,
          vlpCardNumberStr, passwordStr, addressStr, birthdayStr, recaptchaStr);
    }
    // setState(() {
    //   userEmailValidate = false;
    // });
    return true;
  }

  // POST METHOD

  Future<http.Response> postRequest(
      String emailStr,
      String nameStr,
      String lastnameStr,
      String contactNumberStr,
      String vlpCardNumberStr,
      String passwordStr,
      String addressStr,
      String birthdayStr,
      String recaptchaStr) async {
    var url = GeneralVariable.baseUrl + '/api/vlp/users/';

    Map data = {
      'firstname': nameStr,
      'lastname': lastnameStr,
      'birthday': birthdayStr,
      'emailadd': emailStr,
      'password': passwordStr,
      'phoneno': contactNumberStr,
      'city': addressStr,
      'cardno': vlpCardNumberStr,
      'terms': true,
      'recaptcha': recaptchaStr,
    };
    //encode Map to JSON
    var body = json.encode(data);
    print('body  $body');
    var response = await http.post(url,
        headers: {"Content-Type": "application/json"}, body: body);

    print('status${response.statusCode}');
    print('test${response.body}');
    if (response.statusCode == 200) {
      _progressDialog.dismiss();
      showOTP(response.body, emailStr);
    } else {
      _progressDialog.dismiss();
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Error Sign Up!"),
            content: Text(response.body),
          );
        },
      );
    }

    return response;
  }
}
