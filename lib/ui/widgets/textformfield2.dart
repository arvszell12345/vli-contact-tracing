import 'package:flutter/material.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

class CustomTextField2 extends StatelessWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final bool userEmailValidate;

  final bool isSelected = true;

  CustomTextField2({
    this.hint,
    this.textEditingController,
    this.keyboardType,
    this.icon,
    this.userEmailValidate = false,
    this.obscureText = false,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: TextFormField(
        controller: textEditingController,
        keyboardType: keyboardType,
        cursorColor: HexToColor('#EC202A'),
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          //   prefixIcon: Icon(icon, color: Colors.grey, size: 20),
          hintText: hint,
          //   errorText: userEmailValidate ? 'Please enter a Email' : null,
          // border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(30.0),
          //   borderSide: BorderSide(width: 1),
          // ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: HexToColor('#EC202A')),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: HexToColor('#EC202A')),
          ),
        ),
      ),
    );
  }
}
