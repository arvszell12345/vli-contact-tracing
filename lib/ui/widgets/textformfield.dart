import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

class CustomTextField extends StatelessWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final bool userEmailValidate;

  final bool isSelected = true;
  final color;

  CustomTextField({
    this.hint,
    this.textEditingController,
    this.keyboardType,
    this.icon,
    this.userEmailValidate = false,
    this.obscureText = false,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: TextFormField(
        controller: textEditingController,
        keyboardType: keyboardType,
        cursorColor: HexToColor('#EC202A'),
        decoration: InputDecoration(
          prefixIcon: Icon(icon, color: color, size: 20),
          hintText: hint,
          //   errorText: userEmailValidate ? 'Please enter a Email' : null,
          // border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(30.0),
          //   borderSide: BorderSide(width: 1),
          // ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: HexToColor('#EC202A')),
          ),
        ),
      ),
    );
  }
}
