import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';



class PassTextField extends StatefulWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final bool userEmailValidate;

  final bool isSelected = true;
  final color;

  PassTextField({
    this.hint,
    this.textEditingController,
    this.keyboardType,
    this.icon,
    this.userEmailValidate = false,
    this.obscureText = false,
    this.color,
  });

  @override
  _PassTextFieldState createState() => _PassTextFieldState();
}

class _PassTextFieldState extends State<PassTextField> {

  bool _obscureText = true;
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: TextFormField(
        obscureText: _obscureText,
        controller: widget.textEditingController,
        keyboardType: widget.keyboardType,
        cursorColor: HexToColor('#EC202A'),
        decoration: InputDecoration(
          prefixIcon: Icon(widget.icon, color: widget.color, size: 20),
          suffixIcon: IconButton(
            icon: Icon(Icons.remove_red_eye_outlined, color: widget.color,),
            onPressed: _toggle,
          ),
          hintText: widget.hint,
          //   errorText: userEmailValidate ? 'Please enter a Email' : null,
          // border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(30.0),
          //   borderSide: BorderSide(width: 1),
          // ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: HexToColor('#EC202A')),
          ),
        ),
      ),
    );
  }
}
