import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:vli_contact_tracing/utils/supp_file.dart';

class NumberCustomTextField extends StatelessWidget {
  final String hint;
  final TextEditingController textEditingController;
  final TextInputFormatter digitsOnly;
  final TextInputType keyboardType;
  final bool obscureText;
  final IconData icon;
  final bool userEmailValidate;
  final int maxNumber;

  final bool isSelected = true;
  final color;

  NumberCustomTextField(
      {this.hint,
      this.textEditingController,
      this.keyboardType,
      this.icon,
      this.userEmailValidate = false,
      this.obscureText = false,
      this.digitsOnly,
      this.color,
      this.maxNumber});

  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.circular(30.0),
      elevation: 12,
      child: TextFormField(
        inputFormatters: [
          digitsOnly,
          LengthLimitingTextInputFormatter(maxNumber)
        ],
        controller: textEditingController,
        keyboardType: keyboardType,
        cursorColor: HexToColor('#EC202A'),
        decoration: InputDecoration(
          prefixIcon: Icon(icon, color: color, size: 20),
          hintText: hint,
          //   errorText: userEmailValidate ? 'Please enter a Email' : null,
          // border: OutlineInputBorder(
          //   borderRadius: BorderRadius.circular(30.0),
          //   borderSide: BorderSide(width: 1),
          // ),
          enabledBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: new OutlineInputBorder(
            borderRadius: new BorderRadius.circular(30.0),
            borderSide: BorderSide(color: HexToColor('#EC202A')),
          ),
        ),
      ),
    );
  }
}
